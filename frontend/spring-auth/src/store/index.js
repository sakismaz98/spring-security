import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
    user: null
  },

  getters: {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
    authUser: state => state.user
  },

  mutations: {
    auth_request(state) {
      state.status = 'loading';
    },
    login_success(state, token, user) {
      state.status = 'success';
      state.token = token;
      state.user = user;
    },
    auth_success(state) {
      state.status = 'success';
    },
    auth_error(state) {
      state.status = 'error'
    },
    logout(state) {
      state.status = '';
      state.token = '';
    },
    auth_user(state, user) {
      state.user = user;
    }
  },

  actions: {
    // Login action
    login({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit('auth_request');
        // begin axios requst
        axios({
          url: process.env.VUE_APP_AUTH_BASE_URI + '/login',
          data: user,
          method: 'POST'
        }).then(response => {
          const token = `${response.data.tokenType} ${response.data.accessToken}`;
          const user = response.data.user;
          localStorage.setItem('token', token);
          axios.defaults.headers.common['Authorization'] = token;
          commit('login_success', token, user);
          resolve(response);
        })
        .catch(error => {
          commit('auth_error');
          localStorage.removeItem('token');
          console.log(error.response);
          reject(error);
        });
        //end axios request
      });
    },

    // Register action
    register({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit('auth_request');
        axios({
          url: process.env.VUE_APP_AUTH_BASE_URI + '/register',
          data: user,
          method: 'POST'
        }).then(response => {
          resolve(response);
        }).catch(error => {
          console.log(error);
          commit('auth_error', error);
          reject(error);
        });
        // end axios request
      });
    },

    getAuthenticatedUser({ commit }) {
      return new Promise((resolve, reject) => {
        commit('auth_request');
        axios({
          url: process.env.VUE_APP_BASE_URI + '/api/profiles/my_profile',
          method: 'GET'
        }).then(response => {
          const user = response.data;
          commit('auth_success')
          commit('auth_user', user);
          resolve(response);
        }).catch(error => {
          commit('auth_error');
          commit('auth_user', null);
          reject(error);
        });
      });
    },

    logout({ commit }) {
      return new Promise((resolve) => {
        commit('logout');
        localStorage.removeItem('token');
        delete axios.defaults.headers.common['Authorization'];
        resolve();
      });
    },
  },

  modules: {
  }
})
