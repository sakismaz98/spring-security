package com.example.securityJwtProject.model;

import com.example.securityJwtProject.repository.ProfileRepository;
import com.example.securityJwtProject.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Set;

import static org.junit.Assert.*;

@SpringBootTest
class UserFollowingTest {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProfileRepository profileRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Test
    void test_that_user_can_follow_profiles() {
        userRepository.deleteAllInBatch();
        profileRepository.deleteAllInBatch();

        // User Michele with his profile!
        User user = new User("Michele", "Obama", "tester@test.com", "!Password!");
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Profile profile = new Profile();
        profile.setTitle("First Profile");
        profile.setDescription("My First Description");
        profile.setUser(user);
        profileRepository.save(profile);
        userRepository.save(user);

        // Second User with his profile!
        User user1 = new User("Berlin", "laCasa", "lacasaBerlin@test.com", "!Password!");
        user1.setPassword(passwordEncoder.encode(user1.getPassword()));
        Profile profile1 = new Profile();
        profile1.setTitle("La Casa De Papel");
        profile1.setDescription("A Spain Tv Serie");
        profile1.setUser(user1);
        profileRepository.save(profile1);
        userRepository.save(user1);

        // Set That Michele follow berlin
        user.setFollowing(Set.of(profile1));

        // Set that berlin has michele on his followers
        profile1.setFollowers(Set.of(user));
        userRepository.save(user);

        assertEquals(1, user.getFollowing().size());
        assertEquals(0, user1.getFollowing().size());
    }

}