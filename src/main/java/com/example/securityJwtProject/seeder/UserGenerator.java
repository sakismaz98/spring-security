package com.example.securityJwtProject.seeder;


import com.example.securityJwtProject.model.Post;
import com.example.securityJwtProject.model.Profile;
import com.example.securityJwtProject.model.User;
import com.example.securityJwtProject.repository.PostRepository;
import com.example.securityJwtProject.repository.ProfileRepository;
import com.example.securityJwtProject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class UserGenerator implements CommandLineRunner {

    final UserRepository userRepository;
    final ProfileRepository profileRepository;
    final PostRepository postRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public UserGenerator(UserRepository userRepository, ProfileRepository profileRepository, PostRepository postRepository) {
        this.userRepository = userRepository;
        this.profileRepository = profileRepository;
        this.postRepository = postRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        userRepository.deleteAllInBatch();
        profileRepository.deleteAllInBatch();
        postRepository.deleteAllInBatch();


        // User Michele with his profile and post!
        User user = new User("Michele", "Obama", "tester@test.com", "!Password!");
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Post post = new Post();
        post.setContent("Its the first post here so hello");
        Post post1 = new Post();
        post1.setContent("Its the second post here so hello");
        Profile profile = new Profile();
        profile.setTitle("First Profile");
        profile.setDescription("My First Description");
        profile.setUser(user);
        Post post3 = new Post();
        post3.setContent("Third Post");
        profileRepository.save(profile);
        userRepository.save(user);
        post.setUser(user);
        post1.setUser(user);
        post3.setUser(user);
        postRepository.save(post);
        postRepository.save(post1);
        postRepository.save(post3);


        // Second User with his profile!
        User user1 = new User("Berlin", "laCasa", "lacasaBerlin@test.com", "!Password!");
        user1.setPassword(passwordEncoder.encode(user1.getPassword()));
        Post post2 = new Post();
        post2.setContent("3rd Post from different user here heleeeeelo00000ooo");
        Profile profile1 = new Profile();
        profile1.setTitle("La Casa De Papel");
        profile1.setDescription("A Spain Tv Serie");
        profile1.setUser(user1);
        profileRepository.save(profile1);
        userRepository.save(user1);
        post2.setUser(user1);
        postRepository.save(post2);

        // Set That Michele follow berlin
        user.setFollowing(Set.of(profile1));

        // Set that berlin has michele on his followers
        profile1.setFollowers(Set.of(user));
        userRepository.save(user);


        userRepository.save(user);
        userRepository.save(user1);

//        System.out.println(user.getName() + " " + user.getUsername()  + " has " + profile.getFollowers().size() + " followers and follow " + user.getFollowing().size());
//        System.out.println(user1.getName() + " " + user1.getUsername() + " has " + profile1.getFollowers().size() + " followers and follow " + user1.getFollowing().size());
    }
}
