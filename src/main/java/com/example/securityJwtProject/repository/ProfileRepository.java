package com.example.securityJwtProject.repository;

import com.example.securityJwtProject.model.Profile;
import com.example.securityJwtProject.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long> {
    Profile findProfileByUserId(Long id);
}
