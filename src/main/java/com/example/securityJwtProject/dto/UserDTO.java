package com.example.securityJwtProject.dto;

import com.example.securityJwtProject.model.Post;
import com.example.securityJwtProject.model.Profile;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Set;

public class UserDTO {

	private Long id;

	private String username;

	private String name;

	private String email;

	@JsonIgnoreProperties("user")
	private Profile profile;

	@JsonIgnoreProperties("user")
	private Set<Post> posts;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public Set<Post> getPosts() {
		return posts;
	}

	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}
}
