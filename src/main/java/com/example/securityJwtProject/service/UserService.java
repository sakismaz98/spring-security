package com.example.securityJwtProject.service;

import com.example.securityJwtProject.model.User;

import java.util.List;

public interface UserService {

	List<User> getAllUsers();

	User getUserById(Long id);

	User getUserByEmail(String email);

	User getUserByUsername(String email);

	User getUserByUsernameOrEmail(String username, String email);

	Boolean userExistsByUsername(String username);

	Boolean userExistsByEmail(String email);

}

