package com.example.securityJwtProject.service.impl;

import com.example.securityJwtProject.exception.UserNotFoundException;
import com.example.securityJwtProject.model.User;
import com.example.securityJwtProject.repository.UserRepository;
import com.example.securityJwtProject.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;

	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public User getUserById(Long id) {
		return userRepository.findById(id)
				.orElseThrow(() -> new UserNotFoundException("User not found with id: " + id));
	}

	@Override
	public User getUserByEmail(String email) {
		return userRepository.findByEmail(email)
				.orElseThrow(() -> new UserNotFoundException("User not found with email: " + email));
	}

	@Override
	public User getUserByUsername(String email) {
		return null;
	}

	@Override
	public User getUserByUsernameOrEmail(String username, String email) {
		return null;
	}

	@Override
	public Boolean userExistsByUsername(String username) {
		return null;
	}

	@Override
	public Boolean userExistsByEmail(String email) {
		return null;
	}
}
