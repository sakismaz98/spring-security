package com.example.securityJwtProject.controller;

import com.example.securityJwtProject.model.Profile;
import com.example.securityJwtProject.model.User;
import com.example.securityJwtProject.payload.ApiResponse;
import com.example.securityJwtProject.payload.JwtAuthenticationResponse;
import com.example.securityJwtProject.payload.LoginRequest;
import com.example.securityJwtProject.payload.RegisterRequest;
import com.example.securityJwtProject.repository.ProfileRepository;
import com.example.securityJwtProject.repository.UserRepository;
import com.example.securityJwtProject.security.JwtUtil;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProfileRepository profileRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtUtil jwtUtil;

    @GetMapping("/test")
    public ResponseEntity<?> getMessage() {
        return ResponseEntity.ok(new ApiResponse(true, "Access To Resource"));
    }

    @PostMapping("/login")
    public ResponseEntity<?> authenticate(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtil.generateToken(authentication);

        // Consider to refactor JwtAuthenticationResponse
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, authentication));
    }

    // Refactor register method to use service!!!
    @PostMapping("/register")
    public ResponseEntity<?> register(@Valid @RequestBody RegisterRequest registerRequest) {
        if (userRepository.existsByUsername(registerRequest.getUsername())) {
            return new ResponseEntity(new ApiResponse(false, "Username is already in use"), HttpStatus.BAD_REQUEST);
        }

        if (userRepository.existsByEmail(registerRequest.getEmail())) {
            return new ResponseEntity(new ApiResponse(false, "Email is already in use"), HttpStatus.BAD_REQUEST);
        }

        User user = new User(registerRequest.getName(), registerRequest.getUsername(), registerRequest.getEmail(), registerRequest.getPassword());

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        Profile profile = new Profile(); // Create a profile model
        profile.setImage("/img/default-avatar.jpg"); // set Default image
        profile.setUser(user);           // Relate the profile with user
        profileRepository.save(profile); // Save the profile model

        User result = userRepository.save(user);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/users/{username}")
                .buildAndExpand(result.getUsername()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
    }

}
