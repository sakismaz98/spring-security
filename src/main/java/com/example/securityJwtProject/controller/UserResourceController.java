package com.example.securityJwtProject.controller;

import com.example.securityJwtProject.dto.UserDTO;
import com.example.securityJwtProject.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/resource/users")
public class UserResourceController {

	private final ModelMapper modelMapper;
	private final UserService userService;

	public UserResourceController(ModelMapper modelMapper, UserService userService) {
		this.modelMapper = modelMapper;
		this.userService = userService;
	}

	@GetMapping
	public ResponseEntity<?> getAllUsers() {
		return ResponseEntity.ok(
				userService.getAllUsers()
				.stream()
				.map(user -> modelMapper.map(user, UserDTO.class))
				.collect(Collectors.toList())
		);
	}

}
