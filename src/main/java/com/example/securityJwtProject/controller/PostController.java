package com.example.securityJwtProject.controller;

import com.example.securityJwtProject.model.Post;
import com.example.securityJwtProject.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/posts")
public class PostController {

	@Autowired
	private PostRepository postRepository;

	@GetMapping()
	public ResponseEntity<?> getAllPosts() {
		return ResponseEntity.ok(postRepository.findAll());
	}
}
