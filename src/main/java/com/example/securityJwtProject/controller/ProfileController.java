package com.example.securityJwtProject.controller;

import com.example.securityJwtProject.model.Profile;
import com.example.securityJwtProject.payload.ApiResponse;
import com.example.securityJwtProject.property.FileStorageProperties;
import com.example.securityJwtProject.repository.ProfileRepository;
import com.example.securityJwtProject.security.UserPrincipal;
import com.example.securityJwtProject.service.FileStorageService;
import com.sun.istack.Nullable;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.nio.file.FileSystemNotFoundException;

@RestController
@RequestMapping("/api/profiles")
public class ProfileController {

    @Autowired
    ProfileRepository profileRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    FileStorageProperties fileStorageProperties;

    @GetMapping("/my-profile")
    public ResponseEntity<?> getAuthenticatedUserProfile() {
        Authentication authenticatedUser =  SecurityContextHolder.getContext().getAuthentication();

        if (authenticatedUser instanceof AnonymousAuthenticationToken) {
            return new ResponseEntity(new ApiResponse(false, "Unauthorized request"), HttpStatus.UNAUTHORIZED);
        }

        UserPrincipal userPrincipal = (UserPrincipal) authenticatedUser.getPrincipal();

        return ResponseEntity.ok(profileRepository.findProfileByUserId(userPrincipal.getId()));
    }

    // TODO: make it work with @RequestBody instead of @ModelAttribute
    @PutMapping(value = "/profile/edit", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> editAuthenticatedUserProfile(@Valid @ModelAttribute Profile profile, MultipartFile file) {
        Authentication authenticatedUser = SecurityContextHolder.getContext().getAuthentication();

        if (authenticatedUser instanceof AnonymousAuthenticationToken) {
            return new ResponseEntity(
                        new ApiResponse(false, "Unauthorized request"),
                        HttpStatus.UNAUTHORIZED);
        }

        UserPrincipal userPrincipal = (UserPrincipal) authenticatedUser.getPrincipal();

        Profile updatedProfile = profileRepository.findProfileByUserId(userPrincipal.getId());

        updatedProfile.setDescription(profile.getDescription());
        updatedProfile.setTitle(profile.getTitle());
        updatedProfile.setUrl(profile.getUrl());

        if (file.isEmpty()) {
            updatedProfile.setImage("/img/default-avatar.jpg");
        } else {
            String fileName = fileStorageService.storeFile(file);
            String filepath = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path(fileStorageProperties.getUploadDir())
                    .path(fileName)
                    .toUriString();

            updatedProfile.setImage(filepath);
        }

        return ResponseEntity.ok(profileRepository.save(updatedProfile));
    }

}
